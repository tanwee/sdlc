package test.com.movie.jms.listener;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.movie.jm.listener.ConsumerListener;

public class TestConsumerListener {

	private TextMessage message;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		ConsumerListener listener = new ConsumerListener(); 
		listener.onMessage(message);
		assertNull(message);
	}

}