package com.movie.jm.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.movie.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {
	
	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
			
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	ConsumerAdapter consumerAdapter;

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		logger.info("[[TWE]] Inside onMessage");
		
		String json = null;
		if (message instanceof TextMessage) {
			try {
			
					json = ((TextMessage) message).getText();
					logger.info("[[TWE]] Sending to Mongo");
					consumerAdapter.sendToMongo(json);
			
			} catch (JMSException e) {
				
				logger.error("[[TWE]] Message "+json);
				jmsTemplate.convertAndSend(json);
				
			}
			
		catch (UnknownHostException  ex) {
			
			logger.error("[[TWE]] Message "+json);
			jmsTemplate.convertAndSend(json);
			
			}
		}

	}

}
