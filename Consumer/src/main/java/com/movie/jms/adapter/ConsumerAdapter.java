package com.movie.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;


@Component
public class ConsumerAdapter {
	
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	

	public void sendToMongo(String json) throws UnknownHostException {
		// TODO Auto-generated method stub
		logger.info("[[TWE]] Inside SendToMongo");
		
		MongoClient mongoClient = new MongoClient();
		DB db = mongoClient.getDB("vendor");
		DBCollection collection = db.getCollection("contact");
		DBObject myObject = (DBObject) JSON.parse(json);
		collection.insert(myObject);
		logger.info("[[TWE]] Inserted into Mongo");
		mongoClient.close();
		
	}

}
